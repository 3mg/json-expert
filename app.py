# FLASK_APP=app.py FLASK_DEBUG=1 python -m flask run --port 7000

from flask import Flask, render_template, request, abort, send_from_directory, jsonify, Response, send_file, redirect, make_response
import json
import os
from os import listdir, makedirs, rename
from os.path import isfile, join, isdir
import csv
import requests
from flask import flash, redirect, url_for
from werkzeug.utils import secure_filename
from flask_dropzone import Dropzone
import tempfile
from langdetect import detect
from data import get_data, split_data
import zipfile
from io import BytesIO
import time


UPLOAD_FOLDER = 'csv/uploaded'
ALLOWED_EXTENSIONS = {'json', 'csv'}


app = Flask(__name__, static_url_path='')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config.update(
    DROPZONE_ALLOWED_FILE_TYPE='.csv, .json',
    DROPZONE_MAX_FILE_SIZE=15,
    DROPZONE_MAX_FILES=30,
)

dropzone = Dropzone(app)


@app.route('/node_modules/<path:path>')
def send_js(path):
    return send_from_directory('node_modules', path)


@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

@app.route('/csv/<path:path>')
def send_csv(path):
    return send_from_directory('csv', path)


@app.route('/img/<path:path>')
def send_img(path):
    id = request.args.get('id')
    res = request.args.get('res')
    filename = id+'_'+res+'jpg'

    return send_from_directory('cache/img', filename)


@app.route('/')
def index():
    return render_template("index.html",
                           title='Home')


@app.route('/image_cache', methods=['GET', 'POST'])
def image_cache():
    id = request.args.get('id')
    res = request.args.get('res')
    filename = id + '_' + res + '.jpg'

    def download_image(res):
        resp = requests.request(
            method=request.method,
            url='https://img.youtube.com/vi/' + id + '/' + res + '.jpg',
            headers={key: value for (key, value) in request.headers if key != 'Host'},
            data=request.get_data(),
            cookies=request.cookies,
            allow_redirects=False)

        img_data = resp.content
        with open(join('cache', 'img', filename), 'wb') as fh:
            # fh.write(base64.decodebytes(img_data))
            if resp.status_code == 200:
                fh.write(img_data)

        return resp

    # if not isfile(join('cache', 'img', id + '_maxresdefault_.jpg')):
    #     download_image('maxresdefault')

    if isfile(join('cache', 'img', filename)):
        return send_from_directory('cache/img', filename)

    resp = download_image(res)
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for (name, value) in resp.raw.headers.items()
               if name.lower() not in excluded_headers]

    response = Response(resp.content, resp.status_code, headers)
    return response

@app.route('/load', methods=['GET'])
def load():
    data = get_data()

    return jsonify(data['jexcel_configs'])

@app.route('/split', methods=['GET'])
def split():
    data = get_data()

    split_data(data['df'])

    return redirect('/output')

@app.route('/save', methods=['POST'])
def save():
    # with open(args.input_file) as json_file:
    #     json.load(json_file)

    data = request.get_json(silent=True)

    if not isfile(join('csv', data['filename'])):
        with open(join('csv', data['filename']), 'wb') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';',
                                   quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow([col['title'] for col in data['data']['columns']])
            csvwriter.writerows(data['data']['data'])

    with open(join('cache', data['filename'] + '.json'), 'w') as json_file:
        json_file.write(json.dumps(data['data']))

    return jsonify('OK')

@app.route('/load_weights', methods=['GET'])
def load_weights():
    filename = join('static', 'weights.json')
    config = {}

    with open(filename) as json_file:
        data = json.load(json_file)

        table_data = []
        headers = []

        new_headers = set(data.keys()) - set(headers)
        headers = headers + [key for key in data.keys() if key in new_headers]

        table_row = []
        for col in headers:
            table_row.append(data[col] if col in data else '')
        table_data.append(table_row)

        config = {
            'sheetName': filename,
            'csvFileName': filename,
            'data': table_data,
            'columns': [{'title': col} for col in headers],
        }

    return jsonify(config)


@app.route('/save_weights', methods=['POST'])
def save_weights():
    data = request.get_json(silent=True)

    filename = join('static', 'weights.json')
    weights = {}

    for i, col in enumerate(data['data']['columns']):
        weights[col['title']] = int(data['data']['data'][0][i])

    with open(filename, 'w') as json_file:
        json_file.write(json.dumps(weights))

    return jsonify('OK')


@app.route('/delete', methods=['POST', 'DELETE'])
def delete_sheet():
    data = request.get_json(silent=True)

    if not isdir(join('csv', 'deleted')):
        makedirs(join('csv', 'deleted'))

    if isfile(join('csv', data['filename'])):
        rename(join('csv', data['filename']), join('csv', 'deleted', data['filename']))

    return jsonify('OK')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['POST', 'GET'])
def upload():
    if request.method == 'POST':
        f = request.files.get('file')
        f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
    return render_template('index.html')


@app.route('/import_file', methods=['POST'])
def import_file():
    if request.method == 'POST':
        if not isdir(app.config['UPLOAD_FOLDER']):
            makedirs(app.config['UPLOAD_FOLDER'])

        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            if isfile(join('csv', filename)):
                filename = tempfile.mktemp(suffix=".tiff", prefix="filename-", dir="")
            rename(join(app.config['UPLOAD_FOLDER'], filename), join('csv', filename))

    return jsonify('OK')


## SERVE output dir
import os
from pprint import pprint
import magic
import urllib
import sys

mime = magic.Magic(mime=True)

"""
This is the configuration
"""
# To list the current directory of the script, use os.path.dirname(os.path.abspath(__file__))
base_directory = join(os.path.dirname(os.path.abspath(__file__)), 'output')
# These directories will not be listed
ignored_dirs = ["venv"]
ignore_dotfiles = True
ignore_dollarfiles = True
omit_folders = True
omit_files = False


def scan_output_dir():
    files = []
    dirs = []

    for (dirpath, dirnames, filenames) in os.walk(base_directory):
        for name in filenames:
            if omit_files == True:
                break

            for ign in ignored_dirs:
                if ign in dirnames:
                    dirnames.remove(ign)

            nm = os.path.join(dirpath, name).replace(base_directory, "").strip("/").split("/")
            fullpath = os.path.join(dirpath, name)

            if os.path.isfile(fullpath) == False:
                continue

            size = os.stat(fullpath).st_size

            if len(nm) == 1:
                name_s = name.split(".")
                if ignore_dotfiles == True:
                    if name_s[0] == "" or name_s[0] == None:
                        continue

                files.append({
                    "name": name,
                    "size": str(size) + " B",
                    "mime": mime.from_file(fullpath),
                    "fullname": urllib.parse.quote_plus(fullpath)
                })

        for dirname in dirnames:
            if omit_folders == True:
                break

            fullpath = os.path.join(dirpath, dirname)

            if ignore_dotfiles == True:
                name_split = dirname.split(".")
                if name_split[0] == "" or name_split[0] == None:
                    continue

            if ignore_dollarfiles == True:
                name_split = dirname.split("$")
                if name_split[0] == "" or name_split[0] == None:
                    continue

            dirs.append({
                "name": dirname,
                "size": "0b",
                "mime": mime.from_file(fullpath)
            })
    return [files, dirs]

""" The base route with the file list """
@app.route("/output")
def output():
    (files, dirs) = scan_output_dir()

    meta = {
        "current_directory": base_directory
    }

    return render_template("dir_list/index.html", files=sorted(files, key=lambda k: k["name"].lower()), folders=dirs, meta=meta)


@app.route("/download/<filename>")
def download_output(filename):
    filename = urllib.parse.unquote_plus(filename)

    if os.path.isfile(filename):
        if os.path.dirname(filename) == base_directory.rstrip("/"):
            response = make_response(send_file(filename))
            response.headers["Content-Disposition"] = \
                "attachment; " \
                "filenane={ascii_filename};" \
                "filename*=UTF-8''{utf_filename}".format(
                    ascii_filename=os.path.basename(filename).encode('utf8').decode("unicode-escape"),
                    utf_filename=urllib.parse.quote(os.path.basename(filename))
                )
            return response
            # return send_file(filename, as_attachment=True)
        else:
            return render_template("dir_list/no_permission.html")
    else:
        return render_template("dir_list/not_found.html")
    return 'TEST'


@app.route("/download_all")
def download_all():
    (files, dirs) = scan_output_dir()

    memory_file = BytesIO()
    with zipfile.ZipFile(memory_file, 'w') as zf:
        for individualFile in files:
            if os.path.isfile(join('output', individualFile['name'])):
                zf.write(join('output', individualFile['name']))
    memory_file.seek(0)
    return send_file(memory_file, attachment_filename='playlists.zip', as_attachment=True)


if __name__ == '__main__':
    app.run()
