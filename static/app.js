let mySpreadsheets = [];
let weightsSpreadsheet = null;
let persistedData = [];
const currentRows = {};
const currentCols = {};

const videoFormats = '\.(avi|mp4|webm"3gp)';
const audioFormats = '\.(ogg|mp3)';
const vimeoFormats = 'vimeo.*/(\\d+)$';
const linkColumns = {};
let forceSelectionUpdate = true;

function getCurrentTabIndex() {
    return $('.jexcel_tab_link.selected').data('spreadsheet');
}

function getCurrentTab() {
    return mySpreadsheets[getCurrentTabIndex()];
}

function getCurrentTabName() {
    return $('.jexcel_tab_link.selected').text()
}

function persist(instance) {
    const currentTab = getCurrentTab();

    $.ajax('/save', {
        method: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            filename: getCurrentTabName(),
            data: {
                data: currentTab.getData(),
                columns: currentTab.getConfig().columns,
                comments: currentTab.getComments()
            }
        })
    });
}

function persistWeights() {
    $.ajax('/save_weights', {
        method: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            filename: getCurrentTabName(),
            data: {
                data: weightsSpreadsheet.getData(),
                columns: weightsSpreadsheet.getConfig().columns,
                comments: weightsSpreadsheet.getComments()
            }
        })
    });
}

const defaultConfig = {
    // csv: '/csv/20200515a.csv',
    // persistance: '/save',
    csvHeaders: true,
    csvDelimiter: ';',
    tableOverflow: true,
    tableWidth: window.innerWidth + 'px',
    tableHeight: "500px",
    wordWrap: true,
    search: true,
    allowComments: true,
    defaultColWidth: 100,
    includeHeadersOnDownload: true,
    allowExport: true,
    minDimensions: [10, 1],
    filters: true,
    freezeColumns: 1,
    updateTable: async function (instance, cell, col, row, val, id) {
        if (!linkColumns[instance.jexcel.filename] || !linkColumns[instance.jexcel.filename]['external_id']) {
            updateLinkColumns(instance.jexcel.filename, instance.jexcel);
        }

        const linkCol = linkColumns[instance.jexcel.filename]['link'];
        const externalIdCol = linkColumns[instance.jexcel.filename]['external_id'];

        if (col === externalIdCol) {
            if (val) {
                if (val.match(videoFormats)) {

                } else if (val.match(audioFormats)) {

                } else if (val.match(vimeoFormats)) {

                } else {
                    const url = '/image_cache?id=' + encodeURIComponent(val) + '&res=default';
                    const maxresUrl = '/image_cache?id=' + encodeURIComponent(val) + '&res=maxresdefault';
                    cell.innerHTML = val + '<br/><img style="max-width: 100px" src="' + url + '"><img style="display: none" src="' + maxresUrl + '">';
                }
            }
        }

        if (col === linkColumns[instance.jexcel.filename]['thumbnail']) {
            if (val) {
                const id = instance.jexcel.getData()[row][externalIdCol];
                const youtubeThumb = `https://img.youtube.com/vi/${id}/maxresdefault.jpg`;

                if (val && String(val).trim() === youtubeThumb) {
                    const maxresUrl = `/image_cache?id=${id}&res=maxresdefault`;
                    cell.innerHTML = val + `<br/><img style="max-width: 100px" src="${maxresUrl}">`;
                } else if (val) {
                    try {
                        new URL(val);
                        cell.innerHTML = val + '<br/><img style="max-width: 100px" src="' + val + '">';
                    } catch {}
                } else {

                }
            }
        }
    },
    onselection: (instance, x1, y1, x2, y2, origin) => {
        const currentTabName = instance.jexcel.filename;
        const currentRow = currentRows[currentTabName] === undefined ? -1 : currentRows[currentTabName];
        const currentCol = currentCols[currentTabName] === undefined ? -1 : currentCols[currentTabName];

        if (!linkColumns[currentTabName] || !linkColumns[currentTabName]['external_id'] || !linkColumns[currentTabName]['title']) {
            updateLinkColumns(currentTabName, instance.jexcel);
        }
        
        const linkCol = linkColumns[currentTabName]['link'];
        const externalIdCol = linkColumns[currentTabName]['external_id'];
        const titleCol = linkColumns[currentTabName]['title'];

        if (currentRow !== y1 || forceSelectionUpdate) {
            forceSelectionUpdate = false;
            currentRows[currentTabName] = y1;
            currentCols[currentTabName] = x1;
            const rowData = instance.jexcel.getRowData(y1);
            const videoMatch1 = String(rowData[linkCol]).match(videoFormats);
            const videoMatch2 = String(rowData[externalIdCol]).match(videoFormats);
            const audioMatch1 = String(rowData[linkCol]).match(audioFormats);
            const audioMatch2 = String(rowData[externalIdCol]).match(audioFormats);
            const vimeoMatch1 = String(rowData[linkCol]).match(vimeoFormats);
            const vimeoMatch2 = String(rowData[externalIdCol]).match(vimeoFormats);

            const videoMatch = videoMatch1 || videoMatch2;
            const audioMatch = audioMatch1 || audioMatch2;
            const vimeoMatch = vimeoMatch1 || vimeoMatch2;

            if (videoMatch) {
                $('#youtube_widget').html(`<video height="240" controls>
                  <source src="${rowData[videoMatch1 ? linkCol : externalIdCol]}" type="video/${videoMatch[1]}">
                </video>`);
                $('#youtube_preview').html('');
            } else if (audioMatch) {
                $('#youtube_widget').html(`<audio controls>
                  <source src="${rowData[audioMatch1 ? linkCol : externalIdCol]}" type="audio/${audioMatch[1]}">
                </audio>`);
                $('#youtube_preview').html('');
            } else if (vimeoMatch) {
                const videoId = vimeoMatch[1];

                $('#youtube_widget').html(`<iframe src="https://player.vimeo.com/video/${videoId}" width="360" frameborder="0" allow=""></iframe>`);
                $('#youtube_preview').html('');
            } else {
                $('#youtube_widget').html('<iframe src="https://www.youtube.com/embed/' + rowData[externalIdCol] + '?start=10" width="360"  frameborder="0"></iframe>');

                const url = '/image_cache?id=' + encodeURIComponent(rowData[externalIdCol]) + '&res=maxresdefault';
                const urlFallback = '/image_cache?id=' + encodeURIComponent(rowData[externalIdCol]) + '&res=default';
                $('#youtube_preview').html(` <picture style="max-width: 360px">
                      <source media="(min-width: 1px)" type="image/jpg" srcset="${url}">
                      <img style="max-width: 360px" src="${url}" small="${urlFallback}">
                    </object>`);
            }

            $('#video_title').html(rowData[titleCol]);
        }

        if (currentCol !== x1) {
            currentRows[getCurrentTabName()] = y1;
            currentCols[getCurrentTabName()] = x1;
        }
    },
    onload: (el, instance) => {
        instance.updateSelectionFromCoords(0, 0);
        updateLinkColumns(instance.filename, instance);
        $(instance.filter).hide();
    },
    ondeleterow: (instance, y) => {
        // $.ajax('/save', {method: 'DELETE', data: {row: y}})
    },
    onresizecolumn: persist,
    onafterchanges: persist,
    oninsertrow: persist,
    ondeleterow: persist,
    oninsertcolumn: function (instance) {
        updateLinkColumns(getCurrentTabName(), getCurrentTab());
    },
    onfocus: function (instance) {

    },
    onblur: function (instance) {
        const currentTab = instance.jexcel;
        currentTab.updateSelectionFromCoords(currentCols[getCurrentTabName()] >= 0 ? currentCols[getCurrentTabName()] : 0, currentRows[getCurrentTabName()] >= 0 ? currentRows[getCurrentTabName()] : 0);
    },
    onevent: function () {
        console.log(arguments);
    }

};

// var mySpreadsheet = jexcel(document.getElementById('spreadsheet1'), defaultConfig);

$.ajax('/load', {
    method: 'GET'
}).done(function (configs) {
    const sheets = [];
    const filenames = [];

    for (let filename in configs) {
        if (configs.hasOwnProperty(filename)) {
            const config = Object.assign({}, defaultConfig, configs[filename]);

            if (config.columns && config.columns.length > 0) {
                config.columns[0].type = 'numeric';

                for (let i = 0; i < config.columns.length; i++) {
                    if (config.columns[i].title === 'Duration') {
                        config.columns[i].type = 'time';
                    }
                }
            }

            updateSingleSheetLinkColumns(filename, config.columns);
            
            sheets.push(config);
            filenames.push(filename);
        }
    }

    console.log(sheets);
    persistedData = sheets;

    jexcel.tabs(document.getElementById('spreadsheet1'), sheets);

    mySpreadsheets = document.getElementById('spreadsheet1').jexcel;

    for (let i = 0; i < sheets.length; i++) {
        $(mySpreadsheets[i].filter).hide();

        for (let cell in sheets[i].comments) {
            if (sheets[i].comments.hasOwnProperty(cell)) {
                mySpreadsheets[i].setComments(cell, sheets[i].comments[cell]);
            }
        }

        $('#list_select').append($('<option value="' + i + '">' + sheets[i].sheetName + '</option>'));

        mySpreadsheets[i].filename = filenames[i];
    }

    const savedTabIndex = Cookies.get('current_tab_index') || 0;

    $('.jexcel_tab_link').eq(savedTabIndex).click();

    $('#list_select option').eq(savedTabIndex > 0 ? 0 : 1).attr('selected', 'selected');
    updateAppendMode();
    updateTabsSelector();
});

$.ajax('/load_weights', {
    method: 'GET'
}).done(function (_config) {
    const config = Object.assign({}, {
        csvHeaders: true,
        csvDelimiter: ';',
        tableOverflow: true,
        tableWidth: window.innerWidth + 'px',
        tableHeight: "500px",
        wordWrap: true,
        search: false,
        allowComments: false,
        defaultColWidth: 100,
        minDimensions: [10, 1],
        onresizecolumn: persistWeights,
        onafterchanges: persistWeights,
        oninsertrow: persistWeights,
        ondeleterow: persistWeights,
        oninsertcolumn: persistWeights,
    }, _config);

    config.columnDrag = config.allowInsertRow = config.allowInsertColumn = config.allowManualInsertColumn  = config.allowManualInsertRow = false;
    config.allowDeleteRow = config.allowDeleteColumn = config.allowRenameColumn = config.allowManualInsertColumn  = config.allowManualInsertRow = false;

    weightsSpreadsheet = jexcel(document.getElementById('weights_spreadsheet'), config);
});

$('#next').click(() => {
    const currentTab = getCurrentTab();
    const selectedRows = currentTab.getSelectedRows();
    const currentRow = selectedRows.length > 0 ? $(selectedRows[0]).data('y') : -1;

    if (currentRow === currentTab.getData().length - 1) {
        return;
    }

    const nextRow = currentRow >= 0 ? currentRow + 1 : 0;

    currentTab.updateSelectionFromCoords(0, nextRow);

    const content = $(currentTab.el).find('.jexcel_content');
    content.scrollTop(
        $(currentTab.getCellFromCoords(0, nextRow)).position().top - content.position().top
    );
});

$('#prev').click(() => {
    const currentTab = getCurrentTab();
    const selectedRows = currentTab.getSelectedRows();
    const currentRow = selectedRows.length > 0 ? $(selectedRows[0]).data('y') : -1;

    if (currentRow <= 0) {
        return;
    }

    const prevRow = currentRow > 0 ? currentRow - 1 : 0;

    currentTab.updateSelectionFromCoords(0, prevRow);

    const content = $(currentTab.el).find('.jexcel_content');
    content.scrollTop(
        $(currentTab.getCellFromCoords(0, prevRow)).position().top - content.position().top
    );
});

$('#remove').click(() => {
    const currentTab = getCurrentTab();
    $(currentTab.el).focus();

    const selectedRows = currentTab.getSelectedRows();
    const currentRow = selectedRows.length > 0 ? $(selectedRows[0]).data('y') : -1;

    if (currentRow >= 0) {
        currentTab.deleteRow(currentRow, 1);
        currentTab.updateSelectionFromCoords(0, currentRow);
    }
});

$('#move').click(() => {
    const currentTab = getCurrentTab();
    $(currentTab.el).focus();

    const targetTab = mySpreadsheets[$('#list_select').val()];
    const selectedRows = currentTab.getSelectedRows();
    const currentRow = selectedRows.length > 0 ? $(selectedRows[0]).data('y') : -1;

    if (currentRow >= 0) {
        const currentColumns = currentTab.getConfig().columns;
        const targetColumns = targetTab.getConfig().columns;

        for (let i = 0; i < currentColumns.length; i++) {
            if (i >= targetColumns.length) {
                targetTab.insertColumn();
            }

            targetTab.setHeader(i, currentColumns[i].title.trim() !== '' ? currentColumns[i].title.trim() : currentTab.getHeader(i));
        }

        targetTab.insertRow();
        targetTab.setRowData(targetTab.getData().length - 1, currentTab.getRowData(currentRow));
        currentTab.deleteRow(currentRow, 1);

        $('#list_select').val(newOption.val());
        currentTab.updateSelectionFromCoords(0, currentRow);
    }
});

$('#set_titles').click(() => {
    const currentTab = getCurrentTab();
    const rowData = currentTab.getRowData(0);

    for (let i = 0; i < rowData.length; i++) {
        currentTab.setHeader(i, rowData[i].trim() != '' ? rowData[i] : currentTab.getHeader(i));
    }
});

$('#toggle_filters').click(() => {
    const currentTab = getCurrentTab();
    $(currentTab.filter).toggle();
});

$('#appendMode').change(updateAppendMode);
$('#appendMode').prop('checked', Cookies.get('append-mode'));

function updateAppendMode() {
    const checked = $('#appendMode').is(':checked');
    for (var i = 0; i < mySpreadsheets.length; i++) {
        mySpreadsheets[i].appendMode = checked;
    }
    Cookies.set('append-mode', checked);
}

function weightning() {
    return;
    const currentTab = getCurrentTab();
    const columns = currentTab.getConfig().columns;
    let weightColumn = -1;

    for (let i = 0; i < columns.length; i++) {
        if (columns[i].title === 'Weight') {
            weightColumn = i;
            break;
        }
    }

    if (weightColumn === -1) {
        currentTab.insertColumn(undefined, undefined, undefined, {title: 'Weight'});
        weightColumn = columns.length;
    }

    // hideColumn
}

function updateTabsSelector() {
    forceSelectionUpdate = true;

    setTimeout(() => {
        const currentOption = $('#list_select [selected]');

        if (parseInt(currentOption.val()) === getCurrentTabIndex()) {
            const newOption = $('#list_select option').not(currentOption).eq(0);

            currentOption.removeAttr('selected');
            newOption.attr('selected', 'selected');

            $('#list_select').val(newOption.val());
        }

        $('#list_select option').removeAttr('disabled');
        $('#list_select').find('[value="' + getCurrentTabIndex() + '"]').attr('disabled', 'disabled');

        Cookies.set('current_tab_index', getCurrentTabIndex());
    }, 100);
}

function updateSingleSheetLinkColumns(filename, columns) {
    if (!columns) {
        return;
    }

    linkColumns[filename] = {
        'link': 1,
        'external_id': 2,
        'title': 7,
        'thumbnail': 5,
    };

    for (let i = 0; i < columns.length; i++) {
        if (columns[i].title === 'Videolink') {
            linkColumns[filename]['link'] = i;
        } else if (columns[i].title === 'Videolink 2') {
            linkColumns[filename]['external_id'] = i;
        } else if (columns[i].title === 'Title') {
            linkColumns[filename]['title'] = i;
        } else if (columns[i].title === 'Thumbnail') {
            linkColumns[filename]['thumbnail'] = i;
        }
    }
}

function updateLinkColumns(filename, currentTab) {
    updateSingleSheetLinkColumns(filename, currentTab.getConfig().columns);
    currentTab.updateTable();
}

function addSheet() {
    let sheetName = prompt('Create a new sheet', 'New sheet ' + mySpreadsheets.length);

    if (!sheetName) {
        return;
    }

    sheetName += '.csv';

    const new_sheets = [];

    new_sheets.push(Object.assign({}, defaultConfig, {
        sheetName: sheetName,
        csvFileName: sheetName,
        minDimensions: [10, 1]
    }));

    jexcel.tabs(document.getElementById('spreadsheet1'), new_sheets);

    mySpreadsheets = document.getElementById('spreadsheet1').jexcel;
    mySpreadsheets[mySpreadsheets.length - 1].filename = sheetName;

    $('#list_select').append($('<option value="' + (mySpreadsheets.length - 1) + '">' + sheetName + '</option>'));

    updateTabsSelector();
    updateAppendMode();
    persist(mySpreadsheets[mySpreadsheets.length - 1]);
}

async function deleteSheet() {
    const sheetName = getCurrentTabName();
    const currentTabIndex = getCurrentTabIndex();

    if (!confirm(`Are you sure to DELETE ${sheetName} ?`)) {
        return;
    }

    const result = await $.ajax('/delete', {
        method: 'DELETE',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({
            filename: getCurrentTabName(),
        })
    });

    /*jexcel.destroy(getCurrentTab().el);
    mySpreadsheets.splice(currentTabIndex, 1);
    document.getElementById('spreadsheet1').children[1].children[currentTabIndex].remove();
    // $('.jexcel_tab_link.selected').remove();
    // $('.jexcel_tab_link').last().click();
    updateTabsSelector();*/

    alert(`${sheetName} DELETED`);

    location.reload();
}

function downloadJSON() {
    const currentTab = getCurrentTab();
    const filename = currentTab.filename + '.json';

    const data = currentTab.getData();
    const columns = currentTab.getConfig().columns;
    const exportData = [];

    for (let i = 0; i < data.length; i++) {
        const row = data[i];
        const exportRow = {};

        for (let j = 0; j < row.length; j++) {
            exportRow[currentTab.getHeader(j)] = row[j];
        }

        exportData.push(exportRow);
    }

    // Download element
    const blob = new Blob([JSON.stringify(exportData, null, 2)], {type: "octet/stream"});

    // IE Compatibility
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        // Download element
        const pom = document.createElement('a');
        const url = URL.createObjectURL(blob);
        pom.href = url;
        pom.setAttribute('download', filename);
        document.body.appendChild(pom);
        pom.click();
        pom.parentNode.removeChild(pom);
    }

    /*var zip = new JSZip();
    zip.file("Hello.txt", "Hello World\n");
    var img = zip.folder("images");
    img.file("smile.gif", imgData, {base64: true});
    zip.generateAsync({type:"blob"})
    .then(function(content) {
        // see FileSaver.js
        saveAs(content, "example.zip");
    });*/
}

$(document).on('click', '.jexcel_tab_link', updateTabsSelector);

$('#download').click(() => {
    getCurrentTab().download();
});

$('#download_json').click(() => {
    downloadJSON();
});

Dropzone.options.myAwesomeDropzone = {
    paramName: 'file',
    dictDefaultMessage: 'Drop files here to import (json or csv)',
    accept: function(file, done) {
        if (!file.name.match('.+\.(csv|json)')) {
            done('Only csv and json files supported');
        } else { done(); }
    },
    queuecomplete: function () {
        location.reload();
    }
};