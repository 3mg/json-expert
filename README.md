# Installation

```
$ npm install
```

## Using virtualenv

```
$ pip install virtualenv
$ virtualenv venv
$ source ./venv/bin/activate
$ pip install -r requirements.txt
```

## Using anaconda

```
$ conda create -n json python=3.6 anaconda
$ source activate json
$ pip install -r requirements.txt
```

# Running

## Locally
- run ```FLASK_APP=app.py FLASK_DEBUG=1 python -m flask run --port 7000```
- copy your original csv files to "csv" dir
- go to http://127.0.0.1:7000

## Heroku

- setup heroku cli if not yet (https://devcenter.heroku.com/articles/getting-started-with-python#set-up)
- ```heroku create```
- ```heroku buildpacks:add --index 1 heroku/nodejs```
- ```git push heroku master```
- ```heroku ps:scale web=1```
- ```heroku open```