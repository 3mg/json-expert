# FLASK_APP=app.py FLASK_DEBUG=1 python -m flask run --port 7000

from flask import Flask, render_template, request, abort, send_from_directory, jsonify, Response, send_file
import json
import os
import glob
from os import listdir, makedirs, rename
from os.path import isfile, join, isdir
import csv
import requests
from flask import flash, redirect, url_for
from werkzeug.utils import secure_filename
from flask_dropzone import Dropzone
import tempfile
from langdetect import detect
import csv
import pandas
from sklearn import preprocessing
import re


def get_data(calculate_weights=True):
    onlyfiles = [f for f in listdir('csv') if isfile(join('csv', f))]

    configs = {}

    for filename in onlyfiles:
        if isfile(join('cache', filename + '.json')):
            with open(join('cache', filename + '.json')) as json_file:
                configs[filename] = (json.load(json_file))
                configs[filename]['sheetName'] = filename
                configs[filename]['csvFileName'] = filename
        else:
            if filename.rsplit('.', 1)[1].lower() == 'json':
                with open(join('csv', filename)) as json_file:
                    data = json.load(json_file)
                    if isinstance(data, dict) and 'data' in data:
                        data = data['data']
                    if not isinstance(data, list):
                        continue

                    table_data = []
                    headers = []

                    for row in data:
                        new_headers = set(row.keys()) - set(headers)
                        headers = headers + [key for key in row.keys() if key in new_headers]

                    # table_data.append(headers)
                    for row in data:
                        table_row = []
                        for col in headers:
                            table_row.append(row[col] if col in row else '')
                        table_data.append(table_row)

                    configs[filename] = {
                        'sheetName': filename,
                        'csvFileName': filename,
                        'data': table_data,
                        'columns': [{'title': col} for col in headers],
                    }
            else:
                # print(filename)
                with open(join('csv', filename), 'r') as csvfile:
                    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
                    headers = None
                    table_data = []
                    for row in reader:
                        if headers is None:
                            headers = row
                            continue
                        table_data.append(row)

                configs[filename] = {
                    'sheetName': filename,
                    'csvFileName': filename,
                    'data': table_data,
                    'columns': [{'title': col} for col in headers] if headers else [],
                }

    data = {}

    for filename in configs:
        data[filename] = []
        for row in configs[filename]['data']:
            obj = {}
            for i, col in enumerate(configs[filename]['columns']):
                obj[col['title'] if 'title' in col else i] = row[i]
            data[filename].append(obj)

    (df, data) = process_data(data, calculate_weights)

    for filename in data:
        headers = []
        table = []

        for obj in data[filename]:
            new_headers = set(obj.keys()) - set(headers)
            headers = headers + [key for key in obj.keys() if key in new_headers]

        for obj in data[filename]:
            row = []
            for col in headers:
                row.append(obj[col] if col in obj else '')
            table.append(row)

        configs[filename]['data'] = table
        configs[filename]['columns'] = [{'title': col} for col in headers]

    return {'data': data, 'jexcel_configs': configs, 'df': df}


def process_data(data, calculate_weights=False):
    data = fill_missed_values(data)
    df = None

    if (calculate_weights):
        (df, data) = calc_weights(data)

    return [df, data]


def fill_missed_values(data):
    # Detect language
    for filename in data:
        for obj in data[filename]:
            if 'Language' in obj:
                title = obj['Title'].strip() if 'Title' in obj else ''
                description = obj['Description'].strip() if 'Description' in obj else ''
                text = description if description != '' else title
                if text != '' and obj['Language'].strip() == '':
                    obj['Language'] = detect(text)

    return data


def get_authors(data):
    authors = {}
    authors_custom_weights = {}

    for filename in data:
        for obj in data[filename]:
            if 'Author' in obj:
                author_name = obj['Author'].strip()
                author = authors[author_name] if author_name in authors else {'name': author_name, 'count': 0, 'playlists': set(), 'custom_weight': 0}
                author['count'] += 1
                if 'Query' in obj:
                    author['playlists'].add(obj['Query'].strip())
                if author_name in authors_custom_weights:
                    author['custom_weight'] = authors_custom_weights[author_name]
                authors[author_name] = author

    return authors


def get_normalized_data(data):
    columns = ['Filename', 'ID', 'Query', 'Videolink', 'Videolink 2', 'Thumbnail', 'Language', 'Subtitle', 'Doubts',
               'AmatPro', 'Metadata', 'MetaAgreg', 'Duration', 'Title', 'Author', 'Descript', 'CC', '4K',
               'MobileFriendly', 'Views', 'Age', 'Weight', 'Page URL', 'Page Title', 'Date/Time']
    normalized_data = []

    def to_number(v):
        try:
            return int(v)
        except ValueError:
            return 0

    for filename in data:
        for obj in data[filename]:
            normalized_row = [filename]
            for col in columns:
                val = str(obj[col]).strip() if col in obj else ''
                if col == 'Filename':
                    continue
                normalized_row.append(val)
            normalized_data.append(normalized_row)

    df = pandas.DataFrame(data=normalized_data, columns=columns)
    return df


def calc_weights(data):
    #       ['count', 'custom_weight', 'playlists']
    author_weights = [1, 2, 3]

    columns =     ['Filename', 'ID', 'Query', 'Videolink', 'Videolink 2', 'Thumbnail', 'Language', 'Subtitle', 'Doubts', 'AmatPro', 'Metadata', 'MetaAgreg', 'Duration', 'Title', 'Author', 'Descript', 'CC', '4K', 'MobileFriendly', 'Views', 'Age', 'Weight', 'Page URL', 'Page Title', 'Date/Time']
    movies_weights = [0,         5,     0,          0,          0,           4,            1,           0,        0,          2,        3,         0,            2,          1,       2,        1,        1,    1,       1,               3,     3,      0,        0,            0,            0]

    filename = join('static', 'weights.json')
    if isfile(filename):
        with open(filename) as json_file:
            w_data = json.load(json_file)
            for i, col in enumerate(columns):
                if col == 'Filename':
                    continue
                movies_weights[i] = w_data[col]

    df = get_normalized_data(data)

    def to_number(v):
        try:
            return int(v)
        except ValueError:
            return 0

    def to_seconds(v):
        duration = 0
        parts = v.split(':')[::-1]
        duration += to_number(parts[0]) if len(parts) > 0 else 0
        duration += (to_number(parts[1]) * 60) if len(parts) > 1 else 0
        duration += (to_number(parts[2]) * 60 * 60) if len(parts) > 2 else 0
        return duration

    def check_present(v):
        if v != '':
            return -1 if v == 'no' or v == '-' else 1
        else:
            return 0

    authors = get_authors(data)
    adf = pandas.DataFrame(data=authors).T
    adf['playlists'] = adf['playlists'].map(lambda s: len(s))
    del adf['name']
    authors_ranks = adf.rank().mul(
        pandas.Series(data=author_weights, index=['count', 'custom_weight', 'playlists'])).sum(1).rank()

    _df = df.copy()

    _df['Weight'] = 0
    _df['ID'] = _df['ID'].map(lambda val: -to_number(val) if val != '' else 1)
    _df['Duration'] = _df['Duration'].map(lambda val: to_seconds(val))
    _df['CC'] = _df['CC'].map(lambda val: check_present(val))
    _df['4K'] = _df['4K'].map(lambda val: check_present(val))
    _df['MobileFriendly'] = _df['MobileFriendly'].map(lambda val: check_present(val))
    _df['AmatPro'] = _df['AmatPro'].map(lambda val: check_present(val))
    _df['Views'] = _df['Views'].map(lambda val: to_number(val))
    _df['Age'] = _df['Age'].map(lambda val: to_number(val))
    _df['Language'] = _df['Language'].map(lambda val: 1 if val == 'en' else 0)

    doubts = pandas.Series(data=_df['Doubts'], dtype='str')

    for index, value in _df.duplicated('Videolink').iteritems():
        if value:
            doubt = 'Duplicate in {}'.format(_df['Filename'][index])
            if doubt in doubts[index]:
                continue
            doubts[index] = '{}{}'.format(doubts[index] + '\n' if doubts[index] else '', doubt)

    has_thumbnail = pandas.Series(data=[0] * _df['Thumbnail'].size)
    thumbnails_data = pandas.Series(data=[0] * _df['Thumbnail'].size)
    for index, value in _df['Thumbnail'].iteritems():
        if value != '' and re.match(r'.*http.*', value):
            has_thumbnail[index] = 1
            thumbnails_data[index] = value
        elif _df['Videolink 2'][index] != '' and _df['Videolink'][index] != '' and re.match(r'.*youtube.*',
                                                                                            _df['Videolink'][index]):
            path = join('cache', 'img', _df['Videolink 2'][index] + '_maxresdefault.jpg')
            if isfile(path) and os.path.getsize(path) > 0:
                has_thumbnail[index] = 1
                thumbnails_data[index] = 'https://img.youtube.com/vi/{}/maxresdefault.jpg'.format(_df['Videolink 2'][index])
            else:
                has_thumbnail[index] = 0
                thumbnails_data[index] = '–'

    subtitle_counts = pandas.Series(data=[0] * _df['Thumbnail'].size)
    for index, value in _df['CC'].iteritems():
        if value and _df['Subtitle'][index] != '':
            subtitle_counts[index] = 1

    for index, value in _df['Duration'].iteritems():
        doubt = ''
        if value < 60:
            doubt = 'Video shorter than 1 minute'
        elif value > 3600:
            doubt = 'Video longer than 1 hour'

        if doubt and doubt not in doubts[index]:
            doubts[index] = '{}{}'.format(doubts[index] + '\n' if doubts[index] else '', doubt)

    metadata_match = pandas.Series(data=[0] * _df['Thumbnail'].size)
    title_match = pandas.Series(data=[0] * _df['Thumbnail'].size)
    for index, value in _df['Metadata'].iteritems():
        tokens = re.split('\W+', _df['Metadata'][index] if _df['Metadata'][index] else '')
        tokens_present_in_query = False
        tokens_present_in_title = False
        for token in tokens:
            if token in _df['Query'][index]:
                tokens_present_in_query = True
            if token in _df['Title'][index]:
                tokens_present_in_title = True
        if _df['Metadata'][index]:
            metadata_match[index] = 1 if tokens_present_in_query else -1
        else:
            metadata_match[index] = 0
        title_match[index] = 1 if tokens_present_in_title else 0

    _df['Doubts'] = doubts
    _df['Thumbnail'] = has_thumbnail
    _df['Subtitle'] = subtitle_counts
    _df['Author'] = _df['Author'].map(lambda author: authors_ranks[author] if author else -1)
    _df['Title'] = title_match
    _df['Metadata'] = metadata_match

    min_max_scaler = preprocessing.MinMaxScaler()

    rank_df = _df.rank(0, 'dense')
    min_max_scaler.fit(pandas.DataFrame(rank_df['ID']))

    rank_df_scaled = pandas.DataFrame(min_max_scaler.transform(rank_df.astype(float)), columns=rank_df.columns)

    movies_ranks = rank_df_scaled.mul(pandas.Series(data=movies_weights, index=columns)).sum(1).rank()
    # movies_ranks = rank_df.mul(pandas.Series(data=movies_weights, index=columns)).sum(1).rank()

    _df['Weight'] = movies_ranks

    wdf = df.copy()
    wdf['Weight'] = movies_ranks
    wdf['Thumbnail'] = thumbnails_data
    wdf['Doubts'] = doubts

    by_file = wdf.groupby([wdf['Filename']])
    for filename, file_group in by_file:
        _file_group = pandas.DataFrame(data=file_group).reset_index(drop=True)
        for index, value in _file_group['Weight'].iteritems():
            data[filename][index]['Weight'] = value
        for index, value in _file_group['Doubts'].iteritems():
            data[filename][index]['Doubts'] = value
        for index, value in _file_group['Thumbnail'].iteritems():
            data[filename][index]['Thumbnail'] = value

    return [wdf, data]


def split_data(df, clear_dir=False):
    if clear_dir:
        for f in glob.glob("output/*.json"):
            os.remove(f)

    def split_aggregated_columns(row):
        row['Subtitle'] = row['Subtitle'].split('\n')
        row['MetaAgreg'] = row['MetaAgreg'].split('\n')
        return row

    by_file = df.sort_values(by='Weight', ascending=False).groupby([df['Filename']])
    for filename, file_group in by_file:
        by_query = file_group.groupby([df['Query']])
        for query, group in by_query:
            path = filename + ' - ' + (query if query else 'empty query') + '.json'
            data = list(group.drop('Filename', 1).T.to_dict().values())

            data = [split_aggregated_columns(row) for row in data]

            with open(join('output', path), 'w') as json_file:
                json_file.write(json.dumps(data))



